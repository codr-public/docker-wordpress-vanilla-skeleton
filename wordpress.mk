include .env

.PHONY: wp wp-nuke wp-install

## wp	:	Executes `wp cli` command
## 		Doesn't support --flag arguments.
wp:
	docker exec $(shell docker ps --filter name='^/$(PROJECT_NAME)_php' --format "{{ .ID }}") wp $(filter-out $@,$(MAKECMDGOALS))

wp-nuke:
	@make wp "db reset --yes"
	@rm -fr ./web
	# @touch ./web/.gitkeep

wp-install:
	@make composer install
	@make wp "core download --path=web"
	cp ./config/wp/wp-config.php ./web/wp-config.php
	# @make bash "rm -fr ../../web/wp-content/uploads"
	# @make bash "ln -s ../../uploads ./web/wp-content/uploads"
	@make wp "core install --url=${PROJECT_BASE_URL} --title=${PROJECT_NAME} --admin_user=admin --admin_password=admin --admin_email=contact@codr.fr"
	# @make wp "core language install --activate fr_FR"

# wp-update:
	# @make composer install
